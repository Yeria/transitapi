﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace TransitAPI
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IRouteService" in both code and config file together.
    [ServiceContract]
    public interface IRouteService
    {
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            UriTemplate = "GetRouteInfo",
            BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        Route GetRoute(string dptId, string arrId);

        [WebInvoke(Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "GetAllStops")]
        List<Stop> GetAllStops();
    }

    [DataContract]
    public class Stop
    {
        string stop_id = "";
        string stop_name = "";

        [DataMember]
        public string StopID {
            get { return stop_id; }
            set { stop_id = value; }
        }

        [DataMember]
        public string StopName {
            get { return stop_name; }
            set { stop_name = value; }
        }
    }

    [DataContract]
    public class Route
    {
        string routeId = "";
        string routeName = "";
        DateTime startTime;
        DateTime endTime;

        [DataMember]
        public string RouteID {
            get { return routeId; }
            set { routeId = value; }
        }

        [DataMember]
        public string RouteName {
            get { return routeName; }
            set { routeName = value; }
        }

        [DataMember]
        public DateTime DepartureTime {
            get { return startTime; }
            set { startTime = value; }
        }

        [DataMember]
        public DateTime ArrivalTime {
            get { return endTime; }
            set { endTime = value; }
        }
    }
}
