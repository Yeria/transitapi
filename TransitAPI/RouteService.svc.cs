﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace TransitAPI
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "RouteService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select RouteService.svc or RouteService.svc.cs at the Solution Explorer and start debugging.
    public class RouteService : IRouteService
    {
        private string connString = ConfigurationManager.ConnectionStrings["TransitDevDB"].ConnectionString;
        public List<Stop> GetAllStops() {
            return GetStopsFromDB();
        }

        public Route GetRoute(string dptId, string arrId) {
            Route r = new Route();
            r.RouteID = "0787";
            r.RouteName = "Milton GO";
            r.DepartureTime = DateTime.Now.AddMinutes(12);
            r.ArrivalTime = DateTime.Now.AddHours(8);

            return r;
        }

        private List<Stop> GetStopsFromDB() {
            List<Stop> tmpList = new List<Stop>();
            Stop s;

            using (SqlConnection connection = new SqlConnection(connString)) {
                using (SqlCommand cmd = new SqlCommand("SELECT * FROM stops WHERE location_type = 0 and parent_station != ''", connection)) {
                    connection.Open();
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read()) {
                        s = new Stop();
                        s.StopID = reader["stop_id"].ToString();
                        s.StopName = reader["stop_name"].ToString();
                        tmpList.Add(s);
                    }
                }
            }

            return tmpList;
        }
    }
}
